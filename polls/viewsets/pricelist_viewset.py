from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins  import NestedViewSetMixin
from polls.models import PriceList
from polls.serializers.pricelist_serializer import PriclistSerializer


class PricelistViewSet(NestedViewSetMixin, ModelViewSet):
      serializer_class = PriclistSerializer
      queryset = PriceList.objects.all()