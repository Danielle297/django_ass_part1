from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins  import NestedViewSetMixin
from polls.models import Company
from polls.serializers.company_serializer import CompanySerializer


class CompanyViewSet(NestedViewSetMixin, ModelViewSet):
      serializer_class = CompanySerializer
      queryset = Company.objects.all()