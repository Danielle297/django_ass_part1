# -*- coding: utf-8 -*-
from django.test import TestCase, Client
from polls.models import Car, Company, PriceList
import pandas as pd
from django_pandas.io import read_frame


class TestActive_companies_and_name_contains_yod(TestCase):

    def setUp(self):
        self.client = Client()
        self.company1 = Company(company_name=u'עתיד', city= u'ירושלים', phone_number=12345, is_active=True)
        self.company1.save()
        self.company2 = Company(company_name=u'אופקים', city= u'ירושלים', phone_number=12355, is_active=False)
        self.company2.save()
        self.car0 = Car(car_name=u'סיטוראן', price=1200)
        self.car0.save()
        self.car1 = Car(car_name="citroen", price=1200)
        self.car1.save()
        self.car2 = Car(car_name=u'פגו', price=1200)
        self.car2.save()
        self.price_list = PriceList(car=self.car0, price_at_company=155511, company=self.company2)
        self.price_list.save()
        self.price_list = PriceList(car=self.car1, price_at_company=12345, company=self.company1)
        self.price_list.save()
        self.price_list = PriceList(car=self.car2, price_at_company=12311, company=self.company2)
        self.price_list.save()

    def test_amount_of_db_elements(self):

        self.active_and_contains_yod = Company.objects.filter(is_active=True, company_name__contains=u'י')
        self.assertEqual(len(self.active_and_contains_yod), 1)

    def test_equal_df(self):

        self.active_and_contains_yod = Company.objects.filter(is_active=True, company_name__contains=u'י')
        df = read_frame(self.active_and_contains_yod, fieldnames=['company__company_name'])
        df.to_excel('ActiveYod113.xlsx')
        response = self.client.get('/polls/active_companies_and_name_contains_yod')
        file1 = open('output4.xlsx', 'wb')
        for chunk in response.content:
            file1.write(chunk)
        file1.close()
        df2 = pd.read_excel('output4.xlsx')
        # df3 = pd.read_excel('companyPrice')
        self.assertItemsEqual(df, df2)

    def verify_path(self):
        response = self.client.get('/polls/active_companies_and_name_contains_yod/')
        self.assertEqual(response.status_code, 200)

