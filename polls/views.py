# -*- coding: utf-8 -*-
from django.shortcuts import render
import pandas as pd
from django.http import HttpResponse
from numpy import matrix
import json
from pandas.io import data
from django_pandas.io import read_frame
from polls.models import Car,Company,PriceList, CompanyQuerySet, PriceListQuerySet
from pandas import ExcelWriter
import os
from django.core import serializers

# /polls/
# def cars_from_active_company(request):
#     list_of_active_cars = Car.objects.filter(company__is_active=True).distinct()
#     df = read_frame(list_of_active_cars, fieldnames=['car_name', 'company__company_name', 'price'])
#     df.to_excel('activeCompCars.xlsx')
#     file_content = open('activeCompCars.xlsx', 'rb')
#     response = HttpResponse(file_content, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
#     response['Content-Disposition'] = 'attachment; filename=activeCompCars.xlsx'
#     return response

def cars_from_active_company(request):
    list_of_active_cars = Car.objects.is_company_active()
    print list_of_active_cars
    df = read_frame(list_of_active_cars, fieldnames=['car_name', 'company__company_name', 'price'])
    df.to_excel('activeCompCars.xlsx')
    file_content = open('activeCompCars.xlsx', 'rb')
    response = HttpResponse(file_content, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=activeCompCars.xlsx'
    return response

def cars_from_active_company(request):
    list_of_active_cars = Car.objects.is_company_active()
    return HttpResponse(serializers.serialize('json', list_of_active_cars))

# /polls/price_for_one_car/car_name
# def price_for_one_car(request, car_name):
#     list_of_prices = PriceList.objects.filter(car__car_name= car_name).distinct()
#     df = read_frame(list_of_prices, fieldnames=['company__company_name', 'price_at_company'])
#     df.to_excel('carPrice.xlsx')
#     file_content = open('carPrice.xlsx', 'rb')
#     response = HttpResponse(file_content, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
#     response['Content-Disposition'] = 'attachment; filename=carPrice.xlsx'
#     return response


def price_for_one_car(request, car_name):
    list_of_prices = PriceList.objects.car_name(car_name)
    df = read_frame(list_of_prices, fieldnames=['company__company_name', 'price_at_company'])
    df.to_excel('carPrice.xlsx')
    file_content = open('carPrice.xlsx', 'rb')
    response = HttpResponse(file_content, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=carPrice.xlsx'
    return response

def price_for_one_car(request, car_name):
    list_of_prices = PriceList.objects.car_name(car_name)
    return HttpResponse(serializers.serialize('json', list_of_prices, use_natural_foreign_keys=True))

# /polls/pricelist_by_company/company_name
# def pricelist_by_company(request, company_name):
#     list_of_prices = PriceList.objects.filter(company__company_name= company_name).distinct()
#     df = read_frame(list_of_prices, fieldnames=['car__car_name', 'price_at_company'])
#     df.to_excel('companyPrice.xlsx')
#     file_content = open('companyPrice.xlsx', 'rb')
#     response = HttpResponse(file_content, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
#     response['Content-Disposition'] = 'attachment; filename=companyPrice.xlsx'
#     return response

def pricelist_by_company(request, company_name):
    list_of_prices = PriceList.objects.company_name(company_name)
    df = read_frame(list_of_prices, fieldnames=['car__car_name', 'price_at_company'])
    df.to_excel('companyPrice.xlsx')
    file_content = open('companyPrice.xlsx', 'rb')
    response = HttpResponse(file_content, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=companyPrice.xlsx'
    return response


def pricelist_by_company(request, company_name):
    list_of_prices = PriceList.objects.company_name(company_name)
    return HttpResponse(serializers.serialize('json', list_of_prices, use_natural_foreign_keys=True))


# # /polls/active_companies_and_name_contains_yod/
# def active_companies_and_name_contains_yod(request):
#     active_and_contains_yod = Company.objects.filter(is_active=True, company_name__contains=u'י')
#     df = read_frame(active_and_contains_yod, fieldnames=['company_name'])
#     df.to_excel('activeAndYod.xlsx')
#     file_content = open('activeAndYod.xlsx', 'rb')
#     response = HttpResponse(file_content, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
#     response['Content-Disposition'] = 'attachment; filename=activeAndYod.xlsx'
#     return response


# /polls/active_companies_and_name_contains_yod/
def active_companies_and_name_contains_yod(request):
    active_and_contains_yod = Company.objects.contains_yod().is_company_active()
    df = read_frame(active_and_contains_yod, fieldnames=['company_name'])
    df.to_excel('activeAndYod.xlsx')
    file_content = open('activeAndYod.xlsx', 'rb')
    response = HttpResponse(file_content, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=activeAndYod.xlsx'
    return response

def active_companies_and_name_contains_yod(request):
    active_and_contains_yod = Company.objects.contains_yod().is_company_active()
    return HttpResponse(serializers.serialize('json', active_and_contains_yod))


def min_price_for_company(request):
    list_of_prices_for_company = PriceList.objects.min_price_for_company()
    df = read_frame(list_of_prices_for_company, fieldnames=['car__car_name','company__company_name','price_at_company'])
    df.to_excel('MinPriceForCompany.xlsx')
    file_content = open('MinPriceForCompany.xlsx', 'rb')
    response = HttpResponse(file_content, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=MinPriceForCompany.xlsx'
    return response

def min_price_for_company(request):
    list_of_prices_for_company = PriceList.objects.min_price_for_company()
    return HttpResponse(serializers.serialize('json', list_of_prices_for_company, use_natural_foreign_keys=True))

def index(request):
    return render(request,'index.html')