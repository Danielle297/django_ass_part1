# -*- coding: utf-8 -*-
from django.test import TestCase, Client
from polls.models import Car, Company, PriceList
import json
from django.core import serializers


class TestPricelist_by_company2(TestCase):
    def setUp(self):
        self.client = Client()
        self.company1 = Company(company_name=u'עתיד', city= u'ירושלים', phone_number=12345, is_active=True)
        self.company1.save()
        self.company2 = Company(company_name=u'אופקים', city= u'ירושלים', phone_number=12355, is_active=False)
        self.company2.save()
        #self.car1 = Car(car_name=u'סיטוראן', price=1200)
        #self.car1.save()
        self.car1 = Car(car_name="citroen", price=1200)
        self.car1.save()
        self.car2 = Car(car_name=u'פגו', price=1200)
        self.car2.save()
        self.price_list = PriceList(car=self.car1, price_at_company=12345, company=self.company1)
        self.price_list.save()
        self.price_list = PriceList(car=self.car2, price_at_company=12311, company=self.company2)
        self.price_list.save()


    def test_test1(self):
        response = self.client.get(u'/polls/pricelist_by_company/{name}/'.format(name=u'עתיד'))
        self.assertContains(response, 12345)

    def test_amount_of_db_elements(self):
        self.list_of_prices = PriceList.objects.company_name(u'עתיד')
        self.assertEqual(len(self.list_of_prices), 1)

    def verify_path(self):
        request = self.client.get(u'/polls/pricelist_by_company/{name}/'.format(name=u'עתיד'))
        self.assertEqual(request.status_code, 200)
