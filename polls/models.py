# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from model_utils.models import TimeStampedModel
from query_set import CompanyQuerySet, CarQuerySet, PriceListQuerySet


@python_2_unicode_compatible
class Car(TimeStampedModel, models.Model):
    car_name = models.CharField(max_length=200)
    price = models.FloatField(default=0)
    company = models.ManyToManyField('Company', through='PriceList')
    objects = CarQuerySet.as_manager()

    def __str__(self):
        return self.car_name

    def natural_key(self):
        return (self.car_name)



@python_2_unicode_compatible
class Company(models.Model):
    JERUSALEM = 'JR'
    TELAVIV = 'TA'
    HAIFA = 'HA'
    BEERSHEVA = 'BS'
    AFULA = 'AF'

    CITIES = (
        (JERUSALEM, u'ירושלים'),
        (TELAVIV, u'תל-אביב'),
        (HAIFA, u'חיפה'),
        (BEERSHEVA, u'באר-שבע'),
        (AFULA, u'עפולה'),
    )

    company_name = models.CharField(max_length=200)
    city = models.CharField(max_length=100, choices=CITIES)
    phone_number = models.IntegerField(default=0)
    is_active = models.BooleanField()
    objects = CompanyQuerySet.as_manager()

    def __str__(self):
        return self.company_name

    def natural_key(self):
        return (self.company_name)

class PriceList(models.Model):

    car = models.ForeignKey('Car')
    price_at_company = models.FloatField(default=0)
    company = models.ForeignKey('Company')
    objects = PriceListQuerySet.as_manager()

    def __float__(self):
        return self.price_at_company

