from . import views
from polls.viewsets.car_viewset import CarViewSet
from polls.viewsets.company_viewset import CompanyViewSet
from polls.viewsets.pricelist_viewset import PricelistViewSet
from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework_extensions.routers import ExtendedDefaultRouter

admin.autodiscover()
router = ExtendedDefaultRouter()
(
    router.register('car',CarViewSet, base_name='car'),
    router.register('company',CompanyViewSet, base_name='company'),
    router.register('pricelist',PricelistViewSet, base_name='pricelist'),
    router.register('car',CarViewSet, base_name='car').register('company',CompanyViewSet, base_name='company', parents_query_lookups=['car']),
    router.register('company',CompanyViewSet, base_name='company').register('car',CarViewSet, base_name='car', parents_query_lookups=['company']),
    router.register('pricelist',PricelistViewSet, base_name='pricelist').register('company',CompanyViewSet, base_name='company', parents_query_lookups=['pricelist']),
    router.register('pricelist',PricelistViewSet, base_name='pricelist').register('car',CarViewSet, base_name='car', parents_query_lookups=['pricelist']),
)

urlpatterns = [
    url(r'^index/$', views.index , name='index'),
    url('', include(router.urls)),
    url(r'^cars_from_active_company/$', views.cars_from_active_company, name='cars_from_active_company'),
    url(r'^price_for_one_car/(?P<car_name>.+)/$', views.price_for_one_car, name='price_for_one_car'),
    url(r'^pricelist_by_company/(?P<company_name>.+)/$', views.pricelist_by_company, name='pricelist_by_company'),
    url(r'^active_companies_and_name_contains_yod/$', views.active_companies_and_name_contains_yod, name='active_companies_and_name_contains_yod'),
    url(r'^min_price_for_company/$', views.min_price_for_company, name='min_price_for_company'),
]