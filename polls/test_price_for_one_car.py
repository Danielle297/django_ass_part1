# -*- coding: utf-8 -*-
from django.test import TestCase, Client
from polls.models import Car, Company, PriceList
import pandas as pd
from django_pandas.io import read_frame

class TestPrice_for_one_car(TestCase):
    def setUp(self):
        self.client = Client()
        self.company1 = Company(company_name=u'עתיד', city= u'ירושלים', phone_number=12345, is_active=True)
        self.company1.save()
        self.company2 = Company(company_name=u'אופקים', city= u'ירושלים', phone_number=12355, is_active=False)
        self.company2.save()
        #self.car1 = Car(car_name=u'סיטוראן', price=1200)
        #self.car1.save()
        self.car1 = Car(car_name="citroen", price=1200)
        self.car1.save()
        self.car2 = Car(car_name=u'פגו', price=1200)
        self.car2.save()
        self.price_list = PriceList(car=self.car1, price_at_company=12345, company=self.company1)
        self.price_list.save()
        self.price_list = PriceList(car=self.car2, price_at_company=12311, company=self.company2)
        self.price_list.save()



    def test_amount_of_db_elements(self):
        self.list_of_prices = PriceList.objects.filter(car__car_name= "citroen").distinct()
        self.assertEqual(len(self.list_of_prices), 1)

    def test_equal_df(self):
        self.list_of_prices = PriceList.objects.filter(car__car_name= "citroen").distinct()
        print PriceList.objects.all()
        print self.list_of_prices
        df = read_frame(self.list_of_prices, fieldnames=['company__company_name', 'price_at_company'])
        df.to_excel('priceForCar112.xlsx')
        response = self.client.get(u'/polls/price_for_one_car/{name}/'.format(name=u'פגו'))
        file1 = open('output2.xlsx', 'wb')
        for chunk in response.content:
            file1.write(chunk)
        file1.close()
        df2 = pd.read_excel('output2.xlsx')
        # df3 = pd.read_excel('companyPrice')
        self.assertItemsEqual(df, df2)

    def verify_path(self):
        request = self.client.get(u'/polls/price_for_one_car/{name}/'.format(name=u'פגו'))
        self.assertEqual(request.status_code, 200)
