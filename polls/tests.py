# -*- coding: utf-8 -*-
from django.test import TestCase, Client
from rest_framework.test import APITestCase
import json
from polls.models import Car, PriceList, Company


class CarTest(APITestCase):
    def test_status_code(self):
        response = self.client.get('/polls/car/')
        self.assertEqual(response.status_code, 200)

    def test_status_code1(self):
        response = self.client.get('/polls/company/')
        self.assertEqual(response.status_code, 200)

    def test_status_code2(self):
        response = self.client.get('/polls/pricelist/2/car/')
        self.assertEqual(response.status_code, 200)


