# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.db.models import F,Min


class CarQuerySet(models.QuerySet):
    def is_company_active(self):
        return self.filter(company__is_active=True)


class CompanyQuerySet(models.QuerySet):
    def is_company_active(self):
        return self.filter(is_active = True)

    def contains_yod(self):
        return self.filter(company_name__contains=u'י')



class PriceListQuerySet(models.QuerySet):
    def car_name(self, name):
        return self.filter(car__car_name = name)

    def company_name(self, name):
        return self.filter(company__company_name = name)

    def min_price_for_company(self):
        return self.annotate(min_price_for_company = Min('company__pricelist__price_at_company')).filter(price_at_company=F('min_price_for_company')).only('car__car_name','company__company_name','price_at_company')
