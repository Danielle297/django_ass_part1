from rest_framework import serializers
from polls.models import PriceList

class PriclistSerializer(serializers.ModelSerializer):
    class Meta:
        model = PriceList
        fields = ('company','car','price_at_company')