from rest_framework import serializers
from polls.models import Company

class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('company_name','is_active','phone_number','city')