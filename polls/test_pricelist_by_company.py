# -*- coding: utf-8 -*-
from django.test import TestCase, Client
from polls.models import Car, Company, PriceList
import pandas as pd
from django_pandas.io import read_frame


class TestPricelist_by_company(TestCase):

    def setUp(self):
        self.client = Client()
        self.company1 = Company(company_name=u'עתיד', city= u'ירושלים', phone_number=12345, is_active=True)
        self.company1.save()
        self.company2 = Company(company_name=u'אופקים', city= u'ירושלים', phone_number=12355, is_active=False)
        self.company2.save()
        self.car0 = Car(car_name=u'סיטוראן', price=1200)
        self.car0.save()
        self.car1 = Car(car_name="citroen", price=1200)
        self.car1.save()
        self.car2 = Car(car_name=u'פגו', price=1200)
        self.car2.save()
        self.price_list = PriceList(car=self.car0, price_at_company=155511, company=self.company2)
        self.price_list.save()
        self.price_list = PriceList(car=self.car1, price_at_company=12345, company=self.company1)
        self.price_list.save()
        self.price_list = PriceList(car=self.car2, price_at_company=12311, company=self.company2)
        self.price_list.save()

    def test_amount_of_db_elements(self):

        self.list_of_prices = PriceList.objects.filter(company__company_name= u'אופקים').distinct()
        self.assertEqual(len(self.list_of_prices), 2)

    def test_equal_df(self):

        self.list_of_prices = PriceList.objects.filter(company__company_name= u'אופקים').distinct()
        df = read_frame(self.list_of_prices, fieldnames=['car__car_name', 'price_at_company'])
        df.to_excel('priceAtCompany113.xlsx')
        response = self.client.get(u'/polls/pricelist_by_company/{name}/'.format(name=u'אופקים'))
        file1 = open('output3.xlsx', 'wb')
        for chunk in response.content:
            file1.write(chunk)
        file1.close()
        df2 = pd.read_excel('output3.xlsx')
        # df3 = pd.read_excel('companyPrice')
        self.assertItemsEqual(df, df2)

    def verify_path(self):
        response = self.client.get(u'/polls/pricelist_by_company/{name}/'.format(name=u'אופקים'))
        self.assertEqual(response.status_code, 200)

