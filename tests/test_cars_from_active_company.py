# -*- coding: utf-8 -*-
from django.test import TestCase, Client
from polls.models import Car, Company, PriceList
import pandas as pd
from django_pandas.io import read_frame


class TestCars_from_active_company(TestCase):

    def setUp(self):
        self.client = Client()
        self.company1 = Company(company_name=u'עתיד', city= u'ירושלים', phone_number=12345, is_active=True)
        self.company1.save()
        self.company2 = Company(company_name=u'אופקים', city= u'ירושלים', phone_number=12355, is_active=False)
        self.company2.save()
        self.car1 = Car(car_name=u'סיטוראן', price=1200)
        self.car1.save()
        self.car2 = Car(car_name=u'פגו', price=1200)
        self.car2.save()
        self.price_list = PriceList(car=self.car1, price_at_company=12345, company=self.company1)
        self.price_list.save()
        self.price_list = PriceList(car=self.car2, price_at_company=12345, company=self.company2)
        self.price_list.save()



    def test_amount_of_db_elements(self):

        self.list_of_active_cars = Car.objects.filter(company__is_active=True).distinct()
        self.assertEqual(len(self.list_of_active_cars), 1)

    def test_equal_df(self):

        self.list_of_active_cars = Car.objects.filter(company__is_active=True).distinct()
        df = read_frame(self.list_of_active_cars, fieldnames=['car_name', 'company__company_name', 'price'])
        df.to_excel('activeCompCars111.xlsx')
        response = self.client.get('/polls/')
        file1 = open('output.xlsx', 'wb')
        for chunk in response.content:
            file1.write(chunk)
        file1.close()
        df2 = pd.read_excel('output.xlsx')
        # df3 = pd.read_excel('companyPrice')
        self.assertItemsEqual(df, df2)

    def verify_path(self):

        request = self.client.get('/polls/')
        self.assertEqual(request.status_code, 200)
